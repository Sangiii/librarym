from django.urls import path, include
from rest_framework import routers
from . import views 
#from .views import EmployeePostRudView, EmployeePostAPIView

router = routers.DefaultRouter()
router.register('postings', views.EmployeePostView)

urlpatterns = [
    path('',include(router.urls))
]   
