# generic

#from django.db.models import Q
from rest_framework import viewsets
from django.shortcuts import render
from postings.models import EmployeePost
#from .permissions import IsOwnerOrReadOnly
from .serializers import EmployeePostSerializer


class EmployeePostView(viewsets.ModelViewSet): # DetailView CreateView FormView
   queryset = EmployeePost.objects.all()
   serializer_class = EmployeePostSerializer     
