from django.urls import path, include
from rest_framework import routers
from . import views 
#from .views import EmployeePostRudView, EmployeePostAPIView

router = routers.DefaultRouter()
router.register('assignpost', views.AssignPostView)
router.register('bname', views.BnameView)
router.register('employee', views.EmployeeView)

urlpatterns = [
    path('',include(router.urls))
]   
