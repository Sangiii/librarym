from django.conf import settings
from django.db import models
from django.urls import reverse

from rest_framework.reverse import reverse as api_reverse

# django hosts --> subdomain for reverse
class Bname(models.Model):
    bname = models.CharField(max_length=50)
    

    def __str__(self):
        return self.bname

class AssignPost(models.Model):
    # pk aka id --> numbers
    eid         = models.IntegerField(null=True, blank=True)
    ename       = models.CharField(max_length=50)
    bid         = models.IntegerField(null=True, blank=True)
    bname       = models.ForeignKey(Bname, on_delete=models.CASCADE)
    issue       = models.CharField(max_length=120, null=True, blank=True)
    renew       = models.CharField(max_length=120, null=True, blank=True)

    def __str__(self):
        return self.ename

class Employee(models.Model):
    
    assignpost = models.ManyToManyField(AssignPost)

    def __str__(self):
        return str(self.bname)

   # @property
    #def owner(self):
     #   return self.types

    # def get_absolute_url(self):
    #     return reverse("api-postings:post-rud", kwargs={'pk': self.pk}) '/api/postings/1/'
    
    #def get_api_url(self, request=None):
     #   return api_reverse("api-postings:post-rud", kwargs={'pk': self.pk}, request=request)