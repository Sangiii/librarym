# generic

#from django.db.models import Q
from rest_framework import viewsets
from django.shortcuts import render
from api.models import AssignPost, Bname, Employee
#from .permissions import IsOwnerOrReadOnly
from .serializers import AssignPostSerializer, BnameSerializer, EmployeeSerializer


class AssignPostView(viewsets.ModelViewSet): # DetailView CreateView FormView
   queryset = AssignPost.objects.all()
   serializer_class = AssignPostSerializer     

class BnameView(viewsets.ModelViewSet):
	queryset = Bname.objects.all()
	serializer_class = BnameSerializer

class EmployeeView(viewsets.ModelViewSet):
	queryset = Employee.objects.all()
	serializer_class = EmployeeSerializer