
# Create your views here.
# generic

#from django.db.models import Q
from rest_framework import viewsets
from django.shortcuts import render
from .models import BookPost, BookType, Admin
#from .permissions import IsOwnerOrReadOnly
from .serializers import BookPostSerializer, BookTypeSerializer, AdminSerializer



class BookPostView(viewsets.ModelViewSet): # DetailView CreateView FormView
   queryset = BookPost.objects.all()
   serializer_class = BookPostSerializer     

class BookTypeView(viewsets.ModelViewSet): # DetailView CreateView FormView
   queryset = BookType.objects.all()
   serializer_class = BookTypeSerializer

class AdminView(viewsets.ModelViewSet): # DetailView CreateView FormView
   queryset = Admin.objects.all()
   serializer_class = AdminSerializer
